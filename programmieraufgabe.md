Die Datei housing_data.csv enthält Informationen über Immobilienpreise. Visualisieren Sie die Daten, indem Sie wie folgt vorgehen:

1. Öffnen Sie code.min.uni-hamburg.de mit diesen Zugangsdaten: Nutzer "saa8299", Passwort "MS5ioxsp"
1. Laden Sie die csv-Datei mithilfe einer geeigneten Funktion (z.B. pandas).
1. Betrachten Sie die ersten paar Zeilen des Datensatzes, um einen Überblick über die enthaltenen Daten zu erhalten.
1. Überprüfen Sie, ob im Datensatz fehlende Werte sind und behandeln Sie diese entsprechend.
1. Plotten Sie ein Histogramm (z.B. matplotlib) mit x = Preis und y = Anzahl der Immobilien. 
1. Erstellen Sie ein Streudiagramm mit x = Fläche und y = Preis.
1. Erstellen Sie eine Korrelationsmatrix mit den Werten für Preis, Fläche, Räume (bedrooms).